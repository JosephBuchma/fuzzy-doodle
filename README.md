# Task

1. приєднатись до vmware vsphere (vmware esxi)
2. отримати список віртуалок
3. викачати vmdk/vmx для віртуалки

# How I did it

1. [Got example from govmomi](https://github.com/vmware/govmomi/blob/master/examples/datastores/main.go)
2. Read some vmware docs
   [1](https://pubs.vmware.com/vsphere-55/index.jsp?topic=%2Fcom.vmware.wssdk.apiref.doc%2Fvim.VirtualMachine.html),
   [2](https://pubs.vmware.com/vsphere-55/index.jsp#com.vmware.wssdk.apiref.doc/vim.vm.ConfigInfo.html),
   [3](https://pubs.vmware.com/vsphere-55/index.jsp?topic=%2Fcom.vmware.wssdk.apiref.doc%2Fvim.vm.ConfigInfo.DatastoreUrlPair.html)
3. Modified existing code. See diff below

## Install

I assume that golang is alredy installed.

1. Clone this repo
2. Run `make deps`

## Run

1. Set env variables `GOVMOMI_URL`,`GOVMOMI_USERNAME`, `GOVMOMI_PASSWORD` and `GOVMOMI_INSECURE`
2. Run `make run`, and hopefully it will work :)

## Diff against example

```diff
diff --git a/examples/datastores/main.go b/examples/datastores/main.go
index 5a76adf..619da21 100644
--- a/examples/datastores/main.go
+++ b/examples/datastores/main.go
@@ -1,12 +1,9 @@
 /*
 Copyright (c) 2015 VMware, Inc. All Rights Reserved.
-
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
-
     http://www.apache.org/licenses/LICENSE-2.0
-
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@@ -22,6 +19,7 @@ be used to navigate a vSphere inventory structure using govmomi.
 package main
 
 import (
+	"encoding/xml"
 	"flag"
 	"fmt"
 	"net/url"
@@ -32,7 +30,6 @@ import (
 	"github.com/vmware/govmomi"
 	"github.com/vmware/govmomi/find"
 	"github.com/vmware/govmomi/property"
-	"github.com/vmware/govmomi/units"
 	"github.com/vmware/govmomi/vim25/mo"
 	"github.com/vmware/govmomi/vim25/types"
 	"golang.org/x/net/context"
@@ -145,36 +142,48 @@ func main() {
 	// Make future calls local to this datacenter
 	f.SetDatacenter(dc)
 
-	// Find datastores in datacenter
-	dss, err := f.DatastoreList(ctx, "*")
+	// Find vms
+	vmlist, err := f.VirtualMachineList(ctx, "*")
 	if err != nil {
 		exit(err)
 	}
 
 	pc := property.DefaultCollector(c.Client)
 
-	// Convert datastores into list of references
+	// Convert vms into list of references
 	var refs []types.ManagedObjectReference
-	for _, ds := range dss {
-		refs = append(refs, ds.Reference())
+	for _, vm := range vmlist {
+		refs = append(refs, vm.Reference())
 	}
 
-	// Retrieve summary property for all datastores
-	var dst []mo.Datastore
-	err = pc.Retrieve(ctx, refs, []string{"summary"}, &dst)
+	// Retrieve config property for all vms
+	var vms []mo.VirtualMachine
+	err = pc.Retrieve(ctx, refs, []string{"config"}, &vms)
 	if err != nil {
 		exit(err)
 	}
 
-	// Print summary per datastore
+	// Print names
 	tw := tabwriter.NewWriter(os.Stdout, 2, 0, 2, ' ', 0)
-	fmt.Fprintf(tw, "Name:\tType:\tCapacity:\tFree:\n")
-	for _, ds := range dst {
-		fmt.Fprintf(tw, "%s\t", ds.Summary.Name)
-		fmt.Fprintf(tw, "%s\t", ds.Summary.Type)
-		fmt.Fprintf(tw, "%s\t", units.ByteSize(ds.Summary.Capacity))
-		fmt.Fprintf(tw, "%s\t", units.ByteSize(ds.Summary.FreeSpace))
-		fmt.Fprintf(tw, "\n")
+	fmt.Fprintf(tw, "# Name\tUuid\n")
+	for i, vm := range vms {
+		fmt.Fprintf(tw, "%d. %s\t%s\n", i, vm.Config.Name, vm.Config.Uuid)
+	}
+	tw.Flush()
+
+	var index int
+
+	fmt.Println("\nEnter # of VM to print it's vmx (xml-encoded) and vmdk info: ")
+	fmt.Scan(&index)
+
+	vmx, err := xml.Marshal(vms[index])
+	if err != nil {
+		exit(err)
+	}
+	fmt.Fprintf(tw, "vmx:\n%s\n", vmx)
+	fmt.Fprintf(tw, "vmdx name\tvmdx url\n")
+	for _, vmdx := range vms[index].Config.DatastoreUrl {
+		fmt.Fprintf(tw, "%s\t%s\n", vmdx.Name, vmdx.Url)
 	}
 	tw.Flush()
 }
```

Not tested yet, as I'm not able to download trial verision of vsphere so far,
but it compiles, so probably will work :)
