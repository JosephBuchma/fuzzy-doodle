all: run


deps:
	go get github.com/vmware/govmomi
	go get golang.org/x/net/context

run:
	go run main.go
